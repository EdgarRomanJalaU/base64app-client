import React, { Component } from 'react'
import './text-processor.component.css'

export default class TextProcessor extends Component {
  constructor(props){
    super(props)
    this.state = {
      textToProcess: "",
      textProcessed: "",
      errorReceived: false,
    }
  }
  
  changeHandler = (event) => {
    this.setState({
      textToProcess: event.target.value,
    });
  }
  
  clickHandler = () => {
    const { type } = this.props;
    const { textToProcess } = this.state;
    let url = `${import.meta.env.VITE_BASE_URL}api/Base64/${type.toLowerCase()}`;
    let data = {input: textToProcess};
    
    fetch(url, {
      method: 'POST',
      body: JSON.stringify(data),
      headers:{
        'Content-Type': 'application/json'
      }
    })
    .then(res => {
      console.log(res.headers.get('Content-Type'));
      console.log(res.headers.get('Status-Code'));
      return res.json();
    })
    .then(response => {
      
      this.setState(() => {
        return { textProcessed: response.response}
      })
      // if (response.header.get('status-code') == 200){
      //   this.setState(() => {
      //     return { 
      //       textProcessed: response.response,
      //       errorReceived: false,
      //     };
      //   })
      // }else{
      //   this.setState(() => {
      //     return {
      //       textProcessed: response.title, 
      //       errorReceived: true,
      //     };
      //   })
      // }
    })
  }
  
  render() {
    const { type } = this.props;
    const { textProcessed, errorReceived } = this.state;
    const { changeHandler, clickHandler } = this;
    return (
      <div className='TextProcessorWrapper'>
        <h2 className="title">{type}</h2>
        <div className='TextProcessorForm'>
          <textarea name="textToProcess" onChange={changeHandler}></textarea>
          <button className='Button' onClick={clickHandler}>{type}</button>
          <textarea name="textProcessed" readOnly value={textProcessed} style={{ color: errorReceived ? 'rgb(220, 0, 0)' : 'rgba(255, 255, 255, 0.87)' }}></textarea>
        </div>
      </div>
    )
  }
}
