import { Component } from 'react'
import TextProcessor from './components/text-processor/text-processor.component'

class App extends Component{
  constructor(props){
    super(props)
    this.state = {
      response: "",
    }
  }
  render() {
    return (
      <div className='App'>
        <h1>Base64 Encoder and Decoder</h1>
        <TextProcessor type='Encode'/>
        <TextProcessor type='Decode'/>
      </div>
    );
  }
}

export default App
